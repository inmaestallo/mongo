package com.inma.librosmongo;

import com.inma.librosmongo.gui.Controlador;
import com.inma.librosmongo.gui.Modelo;
import com.inma.librosmongo.gui.Vista;
/**
 * @autor:Inma Estallo Puyuelo
 *
 */
public class Principal {
    /**
     * método main() es estático .Inica la aplicación
     * @param args de tipo String[]
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
