package com.inma.librosmongo.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

/**
 * @autor:Inma Estallo Puyuelo
 *
 */
public class Cliente {
    private ObjectId id;
    private String nombreCliente;
    private String apellido;
    private double aniosDeCliente;
    private LocalDate fechaAlta;

    /**
     * Constructor vacio
     */
    public Cliente() {
    }

    /**
     *  geid
     * @return id
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * setid
     * @param id
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * getNombreCliente
     * @return nombreCliente
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * setNombreCliente
     * @param nombreCliente
     */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * getApellido
     * @return apellido
     */
    public String getApellido() {
        return apellido;
    }

    /**
     * setApellido
     * @param apellido
     */
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    /**
     *  getAnioDeCliente
     * @return aniosDeCliente
     */
    public double getAniosDeCliente() {
        return aniosDeCliente;
    }

    /**
     * setAniosDecliente
     * @param aniosDeCliente
     */
    public void setAniosDeCliente(double aniosDeCliente) {
        this.aniosDeCliente = aniosDeCliente;
    }

    /**
     * getFechaAlta
     * @return fechaAlta
     */
    public LocalDate getFechaAlta() {
        return fechaAlta;
    }

    /**
     * setFechaAlta
     * @param fechaAlta
     */
    public void setFechaAlta(LocalDate fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    /**
     * tostring
     * @return  nombreCliente+" : "+apellido ;
     */
    @Override
    public String toString() {
        return  nombreCliente+" : "+apellido ;
    }
}
