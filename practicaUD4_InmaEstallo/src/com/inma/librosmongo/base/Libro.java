package com.inma.librosmongo.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;
/**
 * @autor:Inma Estallo Puyuelo
 *
 */
public class Libro {
    private ObjectId id;
    private String titulo;
    private String autor;
    private double precio;
    private LocalDate fechaCompra;

    /**
     * Constructor de libro vacio
     */
    public Libro() {
    }

    /**
     * getId
     * @return id
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * getAutor
     * @return autor
     */
    public String getAutor() {
        return autor;
    }

    /**
     * setAutor
     * @param autor
     */
    public void setAutor(String autor) {
        this.autor = autor;
    }

    /**
     * setId
     * @param id
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * getTitulo
     * @return titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * setTitulo
     * @param titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * getPrecio
     * @return
     */
    public double getPrecio() {
        return precio;
    }

    /**
     * setPrecio
     * @param precio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * getFechaCompra
     * @return fechaCompra
     */
    public LocalDate getFechaCompra() {
        return fechaCompra;
    }

    /**
     * setFechaCompra
     * @param fechaCompra
     */
    public void setFechaCompra(LocalDate fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    /**
     * toString
     * @return
     */
    @Override
    public String toString() {
        return titulo+" : " + autor ;
    }
}
