package com.inma.librosmongo.gui;

import com.inma.librosmongo.base.Cliente;
import com.inma.librosmongo.base.Libro;
import com.inma.librosmongo.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/**
 * @autor:Inma Estallo Puyuelo
 *
 */
public class Modelo {
    private final static String COLECCION_LIBROS = "Libros";
    private final static String COLECCION_CLIENTES = "Clientes";
    private final static String DATABASE = "Libreria";

    private MongoClient client;
    private MongoDatabase baseDatos;
    private MongoCollection coleccionLibros;
    private MongoCollection coleccionClientes;

    /**
     * Método que conecta con Mongo
     */
    public void conectar() {
        client = new MongoClient();
        baseDatos = client.getDatabase(DATABASE);
        coleccionLibros = baseDatos.getCollection(COLECCION_LIBROS);
        coleccionClientes = baseDatos.getCollection(COLECCION_CLIENTES);
    }

    /**
     * Método que desconecta
     */
    public void desconectar(){
        client.close();
    }

    /**
     * Método que guarda libros
     * @param unLibro
     */
    public void guardarLibro(Libro unLibro) {
        coleccionLibros.insertOne(libroToDocument(unLibro));
    }

    /**
     * Método que guarda clientes
     * @param unCliente
     */
    public void guardarCliente(Cliente unCliente) {
        coleccionClientes.insertOne(clienteToDocument(unCliente));
    }

    /**
     * Método que lista libros
     * @return lista
     */
    public List<Libro> getLibros(){
        ArrayList<Libro> lista = new ArrayList<>();

        Iterator<Document> it = coleccionLibros.find().iterator();
        while(it.hasNext()){
            lista.add(documentToLibro(it.next()));
        }
        return lista;
    }


    /**
     * Método que lista cliente
     * @return lista
     */
    public List<Cliente> getCliente(){
        ArrayList<Cliente> lista = new ArrayList<>();

        Iterator<Document> it = coleccionClientes.find().iterator();
        while(it.hasNext()){
            lista.add(documentToCliente(it.next()));
        }
        return lista;
    }

    /**
     * Listo los librosatendiendo a un 2 criterios basados en expresiones regulares.
     * @param text cadena que quiero que aparezca en titulo y autor
     * @return lista
     */
    public List<Libro> getLibros(String text) {
        ArrayList<Libro> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("titulo", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("autor", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionLibros.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToLibro(iterator.next()));
        }

        return lista;
    }
    /**
     * Listo los cliente atendiendo a un 2 criterios basados en expresiones regulares.
     * @param text cadena que quiero que aparezca en nombreo y apellido
     * @return lista
     */
    public List<Cliente> getClientes(String text) {
        ArrayList<Cliente> lista = new ArrayList<>();

        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre", new Document("$regex", "/*"+text+"/*")));
        listaCriterios.add(new Document("apellido", new Document("$regex", "/*"+text+"/*")));
        query.append("$or", listaCriterios);

        Iterator<Document> iterator = coleccionClientes.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToCliente(iterator.next()));
        }

        return lista;
    }

    /**
     * Método que documenta libro
     * @param unLibro
     * @return documento
     */
    public Document libroToDocument(Libro unLibro){
        Document documento = new Document();
        documento.append("titulo", unLibro.getTitulo());
        documento.append("autor", unLibro.getAutor());
        documento.append("precio", unLibro.getPrecio());
        documento.append("fechaCompra", Util.formatearFecha(unLibro.getFechaCompra()));
        return documento;
    }

    /**
     * Método que documenta un cliente
     * @param unCliente
     * @return documento
     */
    public Document clienteToDocument(Cliente unCliente){
        Document documento = new Document();
        documento.append("nombre", unCliente.getNombreCliente());
        documento.append("apellido", unCliente.getApellido());
        documento.append("aniosCliente", unCliente.getAniosDeCliente());
        documento.append("fechaAlta", Util.formatearFecha(unCliente.getFechaAlta()));
        return documento;
    }

    /**
     *  Método libro
     * @param document
     * @return unLibro
     */
    public Libro documentToLibro(Document document){
        Libro unLibro = new Libro();
        unLibro.setId(document.getObjectId("_id"));
        unLibro.setTitulo(document.getString("titulo"));
        unLibro.setAutor(document.getString("autor"));
        unLibro.setPrecio(document.getDouble("precio"));
        unLibro.setFechaCompra(Util.parsearFecha(document.getString("fechaCompra")));
        return unLibro;
    }

    /**
     * Método cliente
     * @param document
     * @return uncliente
     */
    public Cliente documentToCliente(Document document){
        Cliente unCliente = new Cliente();
        unCliente.setId(document.getObjectId("_id"));
        unCliente.setNombreCliente(document.getString("nombre"));
        unCliente.setApellido(document.getString("apellido"));
        unCliente.setAniosDeCliente(document.getDouble("aniosCliente"));
        unCliente.setFechaAlta(Util.parsearFecha(document.getString("fechaAlta")));
        return unCliente;
    }

    /**
     * Método que modifica un libro
     * @param unLibro
     */
    public void modificarLibro(Libro unLibro) {
        coleccionLibros.replaceOne(new Document("_id", unLibro.getId()), libroToDocument((unLibro)));
    }

    /**
     * Método que modifica un cliente
     * @param unCliente
     */
    public void modificarCliente(Cliente unCliente) {
        coleccionClientes.replaceOne(new Document("_id", unCliente.getId()), clienteToDocument((unCliente)));
    }

    /**
     * Método que borra un libro
     * @param unLibro
     */

    public void borrarLibro(Libro unLibro) {
        coleccionLibros.deleteOne(libroToDocument(unLibro));
    }

    /**
     * Método que borra un cliente
     * @param unCliente
     */
    public void borrarCliente(Cliente unCliente) {
        coleccionClientes.deleteOne(clienteToDocument(unCliente));
    }
}
