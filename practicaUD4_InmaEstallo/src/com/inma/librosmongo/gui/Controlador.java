package com.inma.librosmongo.gui;

import com.inma.librosmongo.base.Cliente;
import com.inma.librosmongo.base.Libro;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
/**
 * @autor:Inma Estallo Puyuelo
 * @see: ActionListener, KeyListener, ListSelectionListener
 */
public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    Vista vista;
    Modelo modelo;

    /**
     * Contructor al que le paso dos parámetros
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        inicializar();
    }

    /**
     * Método que inicia los métodos que tiene dentro
     */
    private void inicializar() {
        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
        modelo.conectar();
        listarLibros(modelo.getLibros());
        listarClientes(modelo.getCliente());
    }

    /**
     * Método que activa los botones
     * @param listener
     */

    private void addActionListeners(ActionListener listener){
        vista.eliminarButton.addActionListener(listener);
        vista.modificarButton.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
        vista.eliminarButtonCliente.addActionListener(listener);
        vista.modificarCliente.addActionListener(listener);
        vista.nuevoButtonCliente.addActionListener(listener);
    }

    /**
     * Método que seleciona la lista
     * @param listener
     */
    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listlibros.addListSelectionListener(listener);
        vista.listCliente.addListSelectionListener(listener);
    }

    /**
     * Método que al escribir en el jlist te biusca el dato
     * @param listener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscar.addKeyListener(listener);
        vista.txtBuscarCliente.addKeyListener(listener);
    }

    /**
     * Método que  activa los botones
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();
        Libro unLibro;
        Cliente unCliente;
        switch (comando){
            case "Nuevo":
                unLibro = new Libro();
                modificarLibroFromCampos(unLibro);
                modelo.guardarLibro(unLibro);
                listarLibros(modelo.getLibros());
                break;
            case "Modificar":
                unLibro = (Libro) vista.listlibros.getSelectedValue();
                modificarLibroFromCampos(unLibro);
                modelo.modificarLibro(unLibro);
                listarLibros(modelo.getLibros());
                break;
            case "Eliminar":
                unLibro = (Libro) vista.listlibros.getSelectedValue();
                modelo.borrarLibro(unLibro);
                listarLibros(modelo.getLibros());
                break;
            case "Nuevo Cliente":
                unCliente= new Cliente();
                modificarClienteFromCampos(unCliente);
                modelo.guardarCliente(unCliente);
                listarClientes(modelo.getCliente());
                break;
            case "Modificar Cliente":
                unCliente = (Cliente) vista.listCliente.getSelectedValue();
                modificarClienteFromCampos(unCliente);
                modelo.modificarCliente(unCliente);
                listarClientes(modelo.getCliente());
                break;
            case "Eliminar CLiente":
                unCliente = (Cliente) vista.listCliente.getSelectedValue();
                modelo.borrarCliente(unCliente);
                listarClientes(modelo.getCliente());
                break;
        }

    }

    /**
     * Método que lista los libros
     * @param lista
     */
    private void listarLibros(List<Libro> lista){
        vista.dlmLibro.clear();
        for (Libro libro : lista){
            vista.dlmLibro.addElement(libro);
        }
    }

    /**
     * Método que lista un lcliente
     * @param lista
     */
    private void listarClientes(List<Cliente> lista){
        vista.dlmCliente.clear();
        for (Cliente cliente : lista){
            vista.dlmCliente.addElement(cliente);
        }
    }

    /**
     * Método que modificar un libro
     * @param unLibro
     */
    private void modificarLibroFromCampos(Libro unLibro) {
        unLibro.setTitulo(vista.txtTitulo.getText());
        unLibro.setAutor(vista.txtNombreAutor.getText());
        unLibro.setPrecio(Double.parseDouble(vista.txtPrecio.getText()));
        unLibro.setFechaCompra(vista.fechaCompra.getDate());
    }

    /**
     * Método que modifica cliente
     * @param unCliente
     */
    private void modificarClienteFromCampos(Cliente unCliente) {
        unCliente.setNombreCliente(vista.txtNombre.getText());
        unCliente.setApellido(vista.txtApellido.getText());
        unCliente.setAniosDeCliente(Double.parseDouble(vista.txtAniosCliente.getText()));
        unCliente.setFechaAlta(vista.fechaCLiente.getDate());
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {

    }

    /**
     * Método para que la lista muerstre los registros que contiene la palbra que busca
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscar){
            listarLibros(modelo.getLibros(vista.txtBuscar.getText()));
        }
        if(e.getSource() == vista.txtBuscarCliente){
            listarClientes(modelo.getClientes(vista.txtBuscarCliente.getText()));
        }
    }

    /**
     * Método que al pinchar un registro del jlis muestra los datos
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {

        if(e.getValueIsAdjusting()){
            Libro unLibro = (Libro) vista.listlibros.getSelectedValue();
            vista.txtTitulo.setText(unLibro.getTitulo());
            vista.txtNombreAutor.setText(unLibro.getAutor());
            vista.txtPrecio.setText(String.valueOf(unLibro.getPrecio()));
            vista.fechaCompra.setDate(unLibro.getFechaCompra());
        }
        if(e.getValueIsAdjusting()){
            Cliente uncliente = (Cliente) vista.listCliente.getSelectedValue();
            vista.txtNombre.setText(uncliente.getNombreCliente());
            vista.txtApellido.setText(uncliente.getApellido());
            vista.txtAniosCliente.setText(String.valueOf(uncliente.getAniosDeCliente()));
            vista.fechaCLiente.setDate(uncliente.getFechaAlta());
        }

    }
}
