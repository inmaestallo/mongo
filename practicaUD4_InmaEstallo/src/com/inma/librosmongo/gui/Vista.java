package com.inma.librosmongo.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.inma.librosmongo.base.Cliente;
import com.inma.librosmongo.base.Libro;

import javax.swing.*;
/**
 * @autor:Inma Estallo Puyuelo
 *
 */
public class Vista {
     JPanel panel1;
    JTextField txtTitulo;
    JTextField txtPrecio;
     JList listlibros;
    JButton nuevoButton;
     JButton modificarButton;
     JButton eliminarButton;
     JTextField txtBuscar;
     DatePicker fechaCompra;
    JTextField txtNombreAutor;
    JTabbedPane tabbedPane1;
    JTextField txtNombre;
    JTextField txtApellido;
    JTextField txtAniosCliente;
     JList listCliente;
    DatePicker fechaCLiente;
    JButton nuevoButtonCliente;
    JButton modificarCliente;
     JButton eliminarButtonCliente;
    JTextField txtBuscarCliente;

    DefaultListModel<Libro> dlmLibro;

    DefaultListModel<Cliente> dlmCliente;


    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        inicializar();
    }

    /**
     * Método que inicializa
     */
    private void inicializar(){
        dlmLibro = new DefaultListModel<>();
        listlibros.setModel(dlmLibro);
        dlmCliente = new DefaultListModel<>();
        listCliente.setModel(dlmCliente);

        fechaCompra.getComponentToggleCalendarButton().setText("Calendario");
        fechaCLiente.getComponentToggleCalendarButton().setText("Calendario");

    }
}
