package com.inma.librosmongo.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
/**
 * @autor:Inma Estallo Puyuelo
 *
 */
public class Util {
    /**
     * Método que formatea la fecha
     * @param fechaMatriculacion
     * @return formateador.format(fechaMatriculacion);
     */
    public static String formatearFecha(LocalDate fechaMatriculacion) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return formateador.format(fechaMatriculacion);
    }

    /**
     * Método que formatea la fecha
     * @param fecha
     * @return  LocalDate.parse(fecha, formateador);
     */
    public static LocalDate parsearFecha(String fecha){
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.parse(fecha, formateador);
    }


}
